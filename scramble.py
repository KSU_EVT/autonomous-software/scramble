#!/usr/bin/env python3
import sys
import os
import shutil
from pathlib import Path
import time
import datetime
import math
import random
import numpy as np

from PIL import *
from PIL import Image, ImageFile
ImageFile.LOAD_TRUNCATED_IMAGES = True

def main():

    directory = os.getcwd()
    directory = os.path.expanduser(directory)
    directory = os.path.expandvars(directory)

    # homedir = Path.home()

    outpath = os.path.join(directory, "randomized")
    if not os.path.exists(outpath):
        os.mkdir(outpath)

    labelsoutpath = os.path.join(outpath, "labels")
    if not os.path.exists(labelsoutpath):
        os.mkdir(labelsoutpath)

    out_files = []
    for root, dirs, files in os.walk(directory, onerror=None):
        for aFile in files:
            ext = aFile.split('.')[-1].lower()
            infile = os.path.join(root, aFile)
            if ext != 'jpg' and ext != 'png' and ext != 'jpeg' or "randomized" in infile or "labels" in infile or aFile[0:1] == ".":
                continue
            outfile = os.path.join(outpath, aFile)
            out_files.append(outfile)

            print(infile)
            print(outfile)

            inimg = Image.open(infile)
            outimg = inimg

            outimg.save(outfile)

            #clean up
            inimg.close()

    for root, dirs, files in os.walk(directory, onerror=None):
        for aFile in files:
            ext = aFile.split('.')[-1].lower()
            infile = os.path.join(root, aFile)
            outfile = os.path.join(labelsoutpath, aFile)
            if "labels" not in infile or "randomized" in infile or aFile[0:1] == "." or ext != 'txt':
                continue
            shutil.copy(infile, outfile)

    # end loop

    # shuffle the images and their labels
    size = len(out_files)
    for i in range(0, size * 4):
        # pick two at random
        A = out_files[random.randint(0, size - 1)]
        B = out_files[random.randint(0, size - 1)]
        A_ext = A.split('.')[-1]
        B_ext = B.split('.')[-1]
        A_path = os.path.join(*A.split(os.path.sep)[:-1])
        A_path = os.path.join(os.path.abspath(os.sep), A_path)
        A_yolo_end = os.path.join("labels", A.split(os.path.sep)[-1].split('.')[-2] + ".txt")
        A_yolo_txt_path = os.path.join(A_path, A_yolo_end)
        B_path = os.path.join(*B.split(os.path.sep)[:-1])
        B_path = os.path.join(os.path.abspath(os.sep), B_path)
        B_yolo_end = os.path.join("labels", B.split(os.path.sep)[-1].split('.')[-2] + ".txt")
        B_yolo_txt_path = os.path.join(B_path, B_yolo_end)
        print(f'swap: {A} with {B}')
        if A == B:
            continue
        # swap the two images
        os.renames(B, os.path.join(B_path, "imgtmp"))
        os.renames(A, B.split('.')[-2] + '.' + A_ext)
        os.renames(os.path.join(B_path, "imgtmp"), A.split('.')[-2] + '.' + B_ext)

        os.renames(B_yolo_txt_path, os.path.join(B_path, "yolotmp"))
        os.renames(A_yolo_txt_path, B_yolo_txt_path)
        os.renames(os.path.join(B_path, "yolotmp"), A_yolo_txt_path)

        # make sure file ext is correct for out_files list
        out_files.remove(A)
        out_files.remove(B)
        out_files.append(A.split('.')[-2] + '.' + B_ext)
        out_files.append(B.split('.')[-2] + '.' + A_ext)
    #end shuffle loop

    segmented = os.path.join(directory, "segmented")
    if not os.path.exists(segmented):
        os.mkdir(segmented)

    # Create segment folders
    for i in range(5):
        segmentFolder = os.path.join(segmented, str(i))
        if not os.path.exists(segmentFolder):
            os.mkdir(segmentFolder)
        segmentLabelsFolder = os.path.join(segmentFolder, "labels")
        if not os.path.exists(segmentLabelsFolder):
            os.mkdir(segmentLabelsFolder)
        segmentImagesFolder = os.path.join(segmentFolder, "images")
        if not os.path.exists(segmentImagesFolder):
            os.mkdir(segmentImagesFolder)

    # Copy images and labels into each segment
    for i in range(len(out_files)):
        segmentFolder = os.path.join(segmented, str(i%5))
        segmentLabelsFolder = os.path.join(segmentFolder, "labels")
        segmentImagesFolder = os.path.join(segmentFolder, "images")
        infile = out_files[i]
        outfile = os.path.join(segmentImagesFolder, infile.split(os.path.sep)[-1])
        # print(f"infile: {infile} outfile: {outfile}")
        shutil.copy(infile, outfile)

        yolopath = os.path.join(*infile.split(os.path.sep)[:-1])
        yolopath = os.path.join(os.path.abspath(os.sep), yolopath)
        yoloend = os.path.join("labels", infile.split(os.path.sep)[-1].split('.')[-2] + ".txt")
        inyolo = os.path.join(yolopath, yoloend)
        outyolo = os.path.join(segmentFolder, yoloend)
        # print(f"inyolo: {inyolo} outyolo: {outyolo}")
        shutil.copy(inyolo, outyolo)


if __name__ == "__main__":
    main()
